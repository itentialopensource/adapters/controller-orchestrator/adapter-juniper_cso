## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Juniper CSO. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Juniper CSO.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Juniper Contrail Services Orchestrator. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">acceptUserUsagePolicy(body, callback)</td>
    <td style="padding:15px">Set the usage policy acceptance information for the current user.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/accept-user-usage-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">Create a role.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/create-role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUsagePolicy(body, callback)</td>
    <td style="padding:15px">Adding the user usage policy url and policy effective date</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/create-usage-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create a user and associate user with specific roles to projects and/or project-groups</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/create-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(body, callback)</td>
    <td style="padding:15px">Delete a role.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/delete-role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserNoProject(body, callback)</td>
    <td style="padding:15px">Delete the user from the current project</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/delete-user-if-no-other-project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRole(body, callback)</td>
    <td style="padding:15px">Edit a role.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/edit-role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editUsagePolicy(body, callback)</td>
    <td style="padding:15px">Editing the user usage policy url and policy effective date</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/edit-usage-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editUser(body, callback)</td>
    <td style="padding:15px">Update user information and roles of the user.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/edit-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthMode(body, callback)</td>
    <td style="padding:15px">Get the authentication method details for the current project and user type</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-auth-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCapabilitiesCatalog(body, callback)</td>
    <td style="padding:15px">Get the capability hierarchical info.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-capabilities-catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectOssId(body, callback)</td>
    <td style="padding:15px">Get the project related with OSS ID.</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-oss-id-project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(body, callback)</td>
    <td style="padding:15px">Get user details</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserCapabilities(body, callback)</td>
    <td style="padding:15px">Get the capabilities including implied capabilities that a logged in user can do based on the keystone roles</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-user-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserUICapabilities(body, callback)</td>
    <td style="padding:15px">Get the UI capabilities that a logged in user can do based on the keystone roles</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/get-user-ui-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAuthMode(body, callback)</td>
    <td style="padding:15px">Set the authentication for the current project and user type</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/set-auth-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthMethodList(callback)</td>
    <td style="padding:15px">Get authentication method list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/authentication-method?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthMethodById(id, callback)</td>
    <td style="padding:15px">Get authentication method by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/authentication-method/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailTemplateList(callback)</td>
    <td style="padding:15px">Get email template list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEmailTemplate(body, callback)</td>
    <td style="padding:15px">Add a new email-template</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailTemplateById(id, callback)</td>
    <td style="padding:15px">Get email template by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEmailTemplate(id, body, callback)</td>
    <td style="padding:15px">Update an existing email-template</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEmailTemplate(id, callback)</td>
    <td style="padding:15px">Delete email template by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEmailTemplateSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/email-template/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderList(callback)</td>
    <td style="padding:15px">Get Identity Provider list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIdentityProvider(body, callback)</td>
    <td style="padding:15px">Add a new Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderById(id, callback)</td>
    <td style="padding:15px">Get Identity Provider by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityProvider(id, body, callback)</td>
    <td style="padding:15px">Update an existing Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityProvider(id, callback)</td>
    <td style="padding:15px">Delete Identity Provider by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIdentityProviderSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/idp/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperatingCompanyList(callback)</td>
    <td style="padding:15px">Get operating company list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/opco?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperatingCompanyById(id, callback)</td>
    <td style="padding:15px">Get operating company by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/opco/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGroupList(callback)</td>
    <td style="padding:15px">Get project group list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/project-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectGroupById(id, callback)</td>
    <td style="padding:15px">Get project group by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/project-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleList(callback)</td>
    <td style="padding:15px">Get role list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleById(id, callback)</td>
    <td style="padding:15px">Get role by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSMTPServerSettingList(callback)</td>
    <td style="padding:15px">Get SMTP Server Setting list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSMTPServerSetting(body, callback)</td>
    <td style="padding:15px">Add a new SMTP Server Setting</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSMTPServerSettingById(id, callback)</td>
    <td style="padding:15px">Get SMTP Server Setting by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSMTPServerSetting(id, body, callback)</td>
    <td style="padding:15px">Update an existing SMTP Server Setting</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSMTPServerSetting(id, callback)</td>
    <td style="padding:15px">Delete SMTP Server Setting by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSMTPServerSettingSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/smtp-server-setting/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsagePolicyList(callback)</td>
    <td style="padding:15px">Get usage policy list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/usage-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsagePolicyById(id, callback)</td>
    <td style="padding:15px">Get usage policy by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/usage-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">Get user list</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserById(id, callback)</td>
    <td style="padding:15px">Get user by id</td>
    <td style="padding:15px">{base_path}/{version}iamsvc/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFmpmEvent(body, callback)</td>
    <td style="padding:15px">create fmpm events.</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/create_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performFmpmActions(body, callback)</td>
    <td style="padding:15px">perform set of action on events</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/event_action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceFmpmAlert(body, callback)</td>
    <td style="padding:15px">force alert</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/force_alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmAlertSummary(body, callback)</td>
    <td style="padding:15px">get Alert Summary</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_alerts_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmAppMetrics(body, callback)</td>
    <td style="padding:15px">get App Metrics</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_app_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmAppPerfParams(body, callback)</td>
    <td style="padding:15px">get Application Performance Parameters</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_application_perf_params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmDeviceData(body, callback)</td>
    <td style="padding:15px">get FMPM Device Data</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_device_data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEvents(body, callback)</td>
    <td style="padding:15px">get Events</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmEvents(body, callback)</td>
    <td style="padding:15px">get FMPM Events</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_fmpm_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmLinkUsage(body, callback)</td>
    <td style="padding:15px">get Link Usage</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_link_usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmPerformanceParams(body, callback)</td>
    <td style="padding:15px">get Performance Parameters</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_perfparmas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmPerformanceParamsTimestamp(body, callback)</td>
    <td style="padding:15px">get Performance Parameters Timestamp</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_perfparmas_ts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmResourceMetrics(body, callback)</td>
    <td style="padding:15px">get Resource Metrics</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_resource_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSdlanPortStatus(body, callback)</td>
    <td style="padding:15px">get LAN device port status data</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sdlan_port_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSdlanTopPorts(body, callback)</td>
    <td style="padding:15px">get LAN device top ports based on various metrics</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sdlan_top_ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSdwanMetrics(body, callback)</td>
    <td style="padding:15px">get sdwan time series Metrics</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sdwan_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSdwanMetricsByTenant(body, callback)</td>
    <td style="padding:15px">get sdwan time series Metrics by Tenant</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sdwan_metrics_by_tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmServiceMetrics(body, callback)</td>
    <td style="padding:15px">get service metrics data</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_service_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSlaEventSummary(body, callback)</td>
    <td style="padding:15px">get event details where SLA are not met</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sla_event_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSlaEventTimeseries(body, callback)</td>
    <td style="padding:15px">Retrieve time series' SLA not met info for a site</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sla_event_ts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSlaScore(body, callback)</td>
    <td style="padding:15px">retrieve Site/App SLA Score for AppQoE/non-AppQoE sites</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_sla_score?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmStorageMetrics(body, callback)</td>
    <td style="padding:15px">get storage metrics data</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_storage_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmSwitchEvents(body, callback)</td>
    <td style="padding:15px">get switch events</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_switch_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmTenantSlaSummary(body, callback)</td>
    <td style="padding:15px">retrieve Tenant SLA Summary</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_tenant_sla_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmTopApplications(body, callback)</td>
    <td style="padding:15px">get Top applications</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_top_applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmTopNAlerts(body, callback)</td>
    <td style="padding:15px">Get top-n entities with max/min alerts</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_top_n_alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmTunnelStatisticsPerSite(body, callback)</td>
    <td style="padding:15px">get a count of tunnels created/deleted per site for a tenant</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_tunnel_statistics_per_site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmTunnelStatisticsPerTenant(body, callback)</td>
    <td style="padding:15px">get a count of tunnels created/deleted for tenant</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/get_tunnel_statistics_per_tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fmpmMonitorService(body, callback)</td>
    <td style="padding:15px">Start/Stop Monitoring a service</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitor_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fmpmProcessAlertStatus(body, callback)</td>
    <td style="padding:15px">Processes alert status by clearing or deleting</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/process_alert_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmNotificationList(callback)</td>
    <td style="padding:15px">Get Alarm Notification list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmNotification(body, callback)</td>
    <td style="padding:15px">Add a new Alarm Notification</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmNotificationById(id, callback)</td>
    <td style="padding:15px">Get Alarm Notification by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmNotification(id, body, callback)</td>
    <td style="padding:15px">Update an existing Alarm Notification</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmNotification(id, callback)</td>
    <td style="padding:15px">Delete Alarm Notification</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmNotificationSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alarm_notification/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertObjectList(callback)</td>
    <td style="padding:15px">Get Alert Object list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertObject(body, callback)</td>
    <td style="padding:15px">Add a new Alert Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertObjectById(id, callback)</td>
    <td style="padding:15px">Get Alert Object by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertObject(id, body, callback)</td>
    <td style="padding:15px">Update an existing Alert Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertObject(id, callback)</td>
    <td style="padding:15px">Delete Alert Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertObjectSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_object/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertStatusObjectList(callback)</td>
    <td style="padding:15px">Get Alert Status Object list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertStatusObject(body, callback)</td>
    <td style="padding:15px">Add a new Alert Status Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertStatusObjectById(id, callback)</td>
    <td style="padding:15px">Get Alert Status Object by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertStatusObject(id, body, callback)</td>
    <td style="padding:15px">Update an existing Alert Status Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertStatusObject(id, callback)</td>
    <td style="padding:15px">Delete Alert Status Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertStatusObjectSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/alert_status_object/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDvpnEventList(callback)</td>
    <td style="padding:15px">Get Dvpn Event list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDvpnEvent(body, callback)</td>
    <td style="padding:15px">Add a new Dvpn Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDvpnEventById(id, callback)</td>
    <td style="padding:15px">Get Dvpn Event by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDvpnEvent(id, body, callback)</td>
    <td style="padding:15px">Update an existing Dvpn Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDvpnEventObject(id, callback)</td>
    <td style="padding:15px">Delete Dvpn Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDvpnEventSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/dvpn_event/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmPolicyConfigList(callback)</td>
    <td style="padding:15px">Get Fmpm Policy Config list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFmpmPolicyConfig(body, callback)</td>
    <td style="padding:15px">Add a new Fmpm Policy Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFmpmPolicyConfigById(id, callback)</td>
    <td style="padding:15px">Get Fmpm Policy Config by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFmpmPolicyConfig(id, body, callback)</td>
    <td style="padding:15px">Update an existing Fmpm Policy Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFmpmPolicyConfig(id, callback)</td>
    <td style="padding:15px">Delete Fmpm Policy Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFmpmPolicyConfigSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/fmpm_policy_config/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapEConfigList(callback)</td>
    <td style="padding:15px">Get Map E Config list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapEConfig(body, callback)</td>
    <td style="padding:15px">Add a new Map E Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapEConfigById(id, callback)</td>
    <td style="padding:15px">Get Map E Config by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMapEConfig(id, body, callback)</td>
    <td style="padding:15px">Update an existing Map E Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMapEConfig(id, callback)</td>
    <td style="padding:15px">Delete Map E Config</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapEConfigSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_config/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapEEventList(callback)</td>
    <td style="padding:15px">Get Map E Event list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapEEvent(body, callback)</td>
    <td style="padding:15px">Add a new Map E Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapEEventById(id, callback)</td>
    <td style="padding:15px">Get Map E Event by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMapEEvent(id, body, callback)</td>
    <td style="padding:15px">Update an existing Map E Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMapEEvent(id, callback)</td>
    <td style="padding:15px">Delete Map E Event</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMapEEventSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/map_e_event/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectList(callback)</td>
    <td style="padding:15px">Get Monitored Object list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObject(body, callback)</td>
    <td style="padding:15px">Add a new Monitored Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectById(id, callback)</td>
    <td style="padding:15px">Get Monitored Object by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObject(id, body, callback)</td>
    <td style="padding:15px">Update an existing Monitored Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObject(id, callback)</td>
    <td style="padding:15px">Delete Monitored Object</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectHostList(callback)</td>
    <td style="padding:15px">Get Monitored Object Host list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectHost(body, callback)</td>
    <td style="padding:15px">Add a new Monitored Object Host</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectHostById(id, callback)</td>
    <td style="padding:15px">Get Monitored Object Host by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectHost(id, body, callback)</td>
    <td style="padding:15px">Update an existing Monitored Object Host</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectHost(id, callback)</td>
    <td style="padding:15px">Delete Monitored Object Host</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectHostSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_host/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectParamList(callback)</td>
    <td style="padding:15px">Get Monitored Object Param list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectParam(body, callback)</td>
    <td style="padding:15px">Add a new Monitored Object Param</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectParamById(id, callback)</td>
    <td style="padding:15px">Get Monitored Object Param by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectParam(id, body, callback)</td>
    <td style="padding:15px">Update an existing Monitored Object Param</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectParam(id, callback)</td>
    <td style="padding:15px">Delete Monitored Object Param</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectParamSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_param/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectServiceList(callback)</td>
    <td style="padding:15px">Get Monitored Object Service list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectService(body, callback)</td>
    <td style="padding:15px">Add a new Monitored Object Service</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectServiceById(id, callback)</td>
    <td style="padding:15px">Get Monitored Object Service by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectService(id, body, callback)</td>
    <td style="padding:15px">Update an existing Monitored Object Service</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectService(id, callback)</td>
    <td style="padding:15px">Delete Monitored Object Service</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectServiceSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_service/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectTypeList(callback)</td>
    <td style="padding:15px">Get Monitored Object Type list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectType(body, callback)</td>
    <td style="padding:15px">Add a new Monitored Object Type</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectTypeById(id, callback)</td>
    <td style="padding:15px">Get Monitored Object Type by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectType(id, body, callback)</td>
    <td style="padding:15px">Update an existing Monitored Object Type</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectType(id, callback)</td>
    <td style="padding:15px">Delete Monitored Object Type</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectTypeSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/monitored_object_type/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleActionsList(callback)</td>
    <td style="padding:15px">Get Rule Actions list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRuleActions(body, callback)</td>
    <td style="padding:15px">Add a new Rule Actions</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleActionsById(id, callback)</td>
    <td style="padding:15px">Get Rule Actions by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRuleActions(id, body, callback)</td>
    <td style="padding:15px">Update an existing Rule Actions</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRuleActions(id, callback)</td>
    <td style="padding:15px">Delete Rule Actions</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRuleActionsSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_actions/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleMatchersList(callback)</td>
    <td style="padding:15px">Get Rule Matchers list</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRuleMatchers(body, callback)</td>
    <td style="padding:15px">Add a new Rule Matchers</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleMatchersById(id, callback)</td>
    <td style="padding:15px">Get Rule Matchers by id</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRuleMatchers(id, body, callback)</td>
    <td style="padding:15px">Update an existing Rule Matchers</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRuleMatchers(id, callback)</td>
    <td style="padding:15px">Delete Rule Matchers</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRuleMatchersSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}fmpm-provider/rule_matchers/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyExFirewallIntent(body, callback)</td>
    <td style="padding:15px">create intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-ex-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyExFirewallPolicy(body, callback)</td>
    <td style="padding:15px">create policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-ex-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyFirewallIntent(body, callback)</td>
    <td style="padding:15px">create intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyFirewallPolicy(body, callback)</td>
    <td style="padding:15px">create policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySdwanIntent(body, callback)</td>
    <td style="padding:15px">create intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-sdwan-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySslproxyIntent(body, callback)</td>
    <td style="padding:15px">create intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/create-sslproxy-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyExFirewallIntent(body, callback)</td>
    <td style="padding:15px">delete intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-ex-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyExFirewallPolicy(body, callback)</td>
    <td style="padding:15px">delete policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-ex-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyFirewallIntent(body, callback)</td>
    <td style="padding:15px">delete intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyFirewallPolicy(body, callback)</td>
    <td style="padding:15px">delete policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyRule(body, callback)</td>
    <td style="padding:15px">Deletes an existing rule from a policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicySdwanIntent(body, callback)</td>
    <td style="padding:15px">delete intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-sdwan-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicySslproxyIntent(body, callback)</td>
    <td style="padding:15px">delete intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/delete-sslproxy-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPolicyFirewallPolicies(body, callback)</td>
    <td style="padding:15px">Deploy policy to all sites</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/deploy-firewall-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPolicy(body, callback)</td>
    <td style="padding:15px">Deploy policy to all sites</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/deploy-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployPolicyJob(body, callback)</td>
    <td style="padding:15px">called from the job manager as scheduled</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/deploy-policy-job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDeployJobDetails(body, callback)</td>
    <td style="padding:15px">called to get the details of job like device_uuid site_uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-deploy-job-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyExFirewallIntentByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve intent of a policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-ex-firewall-intent-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyExFirewallIntentsForPolicy(body, callback)</td>
    <td style="padding:15px">Retrieve all intents of a policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-ex-firewall-intents-for-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyExFirewallPolicies(body, callback)</td>
    <td style="padding:15px">Retrieve all policies</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-ex-firewall-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyExFirewallPolicyByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve the policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-ex-firewall-policy-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFirewallIntentByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve intent of a policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-firewall-intent-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFirewallIntentsForPolicy(body, callback)</td>
    <td style="padding:15px">Retrieve all intents of a policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-firewall-intents-for-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFirewallPolicies(body, callback)</td>
    <td style="padding:15px">Retrieve all policies.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-firewall-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFirewallPolicyByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve the policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-firewall-policy-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySdwanIntentByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve intent of a policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sdwan-intent-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySdwanIntentsForPolicy(body, callback)</td>
    <td style="padding:15px">Retrieve all intents of a policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sdwan-intents-for-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySdwanPolicies(body, callback)</td>
    <td style="padding:15px">Retrieve all policies</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sdwan-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySdwanPolicyByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve the policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sdwan-policy-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySslproxyIntentByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve intent of a policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sslproxy-intent-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySslproxyIntentsForPolicy(body, callback)</td>
    <td style="padding:15px">Retrieve all intents of a policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sslproxy-intents-for-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySslproxyPolicies(body, callback)</td>
    <td style="padding:15px">Retrieve all policies</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sslproxy-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySslproxyPolicyByUuid(body, callback)</td>
    <td style="padding:15px">Retrieve the policy by uuid</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/get-sslproxy-policy-by-uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyExFirewallIntent(body, callback)</td>
    <td style="padding:15px">update intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-ex-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyExFirewallPolicy(body, callback)</td>
    <td style="padding:15px">update policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-ex-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyFirewallIntent(body, callback)</td>
    <td style="padding:15px">update intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-firewall-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyFirewallPolicy(body, callback)</td>
    <td style="padding:15px">update policy with site assignment.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-firewall-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicySdwanIntent(body, callback)</td>
    <td style="padding:15px">update intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-sdwan-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicySslproxyIntent(body, callback)</td>
    <td style="padding:15px">update intent for the policy.</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/update-sslproxy-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntentPolicy(callback)</td>
    <td style="padding:15px">Get Intent Policy list</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntentPolicy(body, callback)</td>
    <td style="padding:15px">Add a new Intent Policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntentPolicyById(id, callback)</td>
    <td style="padding:15px">Get Intent Policy by id</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntentPolicy(id, body, callback)</td>
    <td style="padding:15px">Update an existing Intent Policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntentPolicy(id, callback)</td>
    <td style="padding:15px">Delete Intent Policy</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntentPolicySearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/intent_policy/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySiteList(callback)</td>
    <td style="padding:15px">Get Policy Site List list</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySiteList(body, callback)</td>
    <td style="padding:15px">Add a new Policy Site List</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySiteListById(id, callback)</td>
    <td style="padding:15px">Get Policy Site List by id</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicySiteList(id, body, callback)</td>
    <td style="padding:15px">Update an existing Policy Site List</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicySiteList(id, callback)</td>
    <td style="padding:15px">Delete Policy Site List</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">creatPolicySiteListSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}policy-mgmt/policy_site_list/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTssmDeleteLanSegments(body, callback)</td>
    <td style="padding:15px">Adds/Deletes LAN segments to devices</td>
    <td style="padding:15px">{base_path}/{version}/tssm/add-delete-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTssmDepartmentToSite(body, callback)</td>
    <td style="padding:15px">Adds LAN configurations on devices at sites</td>
    <td style="padding:15px">{base_path}/{version}/tssm/add-department-to-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTssmLanSegments(body, callback)</td>
    <td style="padding:15px">Adds/Deletes LAN segments to device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/add-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">analyzeTssmSiteUpgrade(body, callback)</td>
    <td style="padding:15px">analyze the site upgrade impacts, estimated time for upgrade and suggest recommendations</td>
    <td style="padding:15px">{base_path}/{version}/tssm/analyze_site_upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTssmPortConfigAssociation(body, callback)</td>
    <td style="padding:15px">Create or update port config associations.</td>
    <td style="padding:15px">{base_path}/{version}/tssm/apply-port-config-association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTssmCsoLicense(body, callback)</td>
    <td style="padding:15px">assign CSO License to tenant</td>
    <td style="padding:15px">{base_path}/{version}/tssm/assign-cso-license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureTssmSites(body, callback)</td>
    <td style="padding:15px">configure sites for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/tssm/configure-sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTssmDvpnTunnels(body, callback)</td>
    <td style="padding:15px">create dvpn tunnels between the sites</td>
    <td style="padding:15px">{base_path}/{version}/tssm/create-dvpn-tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTssmSites(body, callback)</td>
    <td style="padding:15px">create sites with specific requirements.</td>
    <td style="padding:15px">{base_path}/{version}/tssm/create-sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTssmVpnInterconnect(body, callback)</td>
    <td style="padding:15px">Create VPN interconnect between SDWAN provider hub and PE device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/create-vpn-interconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTssmDepartmentFromSite(body, callback)</td>
    <td style="padding:15px">Deletes LAN configurations on devices at sites</td>
    <td style="padding:15px">{base_path}/{version}/tssm/delete-department-from-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTssmDvpnTunnels(body, callback)</td>
    <td style="padding:15px">delete dvpn tunnels between the sites</td>
    <td style="padding:15px">{base_path}/{version}/tssm/delete-dvpn-tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTssmLanSegments(body, callback)</td>
    <td style="padding:15px">Deletes LAN segments to device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/delete-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTssmVpnInterconnect(body, callback)</td>
    <td style="padding:15px">Delete VPN interconnect between SDWAN provider hub and PE device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/delete-vpn-interconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployTssmLanSegments(body, callback)</td>
    <td style="padding:15px">Deploy LAN segments to device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/deploy-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTssmLanSegments(body, callback)</td>
    <td style="padding:15px">Modifies LAN segments to device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/edit-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTssmUpgradeAnalysisReport(body, callback)</td>
    <td style="padding:15px">fetch upgrade analysis report</td>
    <td style="padding:15px">{base_path}/{version}/tssm/get_upgrade_analysis_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">onboardTssmCsoLicense(body, callback)</td>
    <td style="padding:15px">Creates Purchase Order in ems for specified tenant</td>
    <td style="padding:15px">{base_path}/{version}/tssm/onboard-cso-license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">onboardTssmTenant(body, callback)</td>
    <td style="padding:15px">Creates tenant in IAM, customer and sites in TSSM, allocates ip-pool for data and management per site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/onboard-tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reassignTssmLanSegments(body, callback)</td>
    <td style="padding:15px">Reassign LAN segments to devices</td>
    <td style="padding:15px">{base_path}/{version}/tssm/reassign-lan-segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTssmSite(body, callback)</td>
    <td style="padding:15px">remove site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/remove-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewTssmCrlFile(body, callback)</td>
    <td style="padding:15px">renew crl file.</td>
    <td style="padding:15px">{base_path}/{version}/tssm/renew-crl-file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewTssmSiteCert(body, callback)</td>
    <td style="padding:15px">renew site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/renew-site-cert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rmaTssmDevices(body, callback)</td>
    <td style="padding:15px">Taking defaulted device out of service so that it can be replaced later with new device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/rma-devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rmaTssmGrant(body, callback)</td>
    <td style="padding:15px">Taking defaulted devices out of service, backing up device state and configs, and swapping it out with another device</td>
    <td style="padding:15px">{base_path}/{version}/tssm/rma-grant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTssmDeviceActivation(body, callback)</td>
    <td style="padding:15px">Start device activation</td>
    <td style="padding:15px">{base_path}/{version}/tssm/start-device-activation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTssmDeviceBootstrap(body, callback)</td>
    <td style="padding:15px">Start device bootstrap</td>
    <td style="padding:15px">{base_path}/{version}/tssm/start-device-bootstrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTssmSiteToSiteLinks(body, callback)</td>
    <td style="padding:15px">Reconfigure static links between sites</td>
    <td style="padding:15px">{base_path}/{version}/tssm/update-site-to-site-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeTssmSite(body, callback)</td>
    <td style="padding:15px">Upgrade the site(s) from the CSO version in which it was created to the current CSO version</td>
    <td style="padding:15px">{base_path}/{version}/tssm/upgrade-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessProfile(callback)</td>
    <td style="padding:15px">Get Access Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessProfile(body, callback)</td>
    <td style="padding:15px">Add a new Access Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessProfileById(id, callback)</td>
    <td style="padding:15px">Get Access Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Access Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessProfile(id, callback)</td>
    <td style="padding:15px">Delete Access Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessProfileAssociation(callback)</td>
    <td style="padding:15px">Get Access Profile Association list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessProfileAssociation(body, callback)</td>
    <td style="padding:15px">Add a new Access Profile Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessProfileAssociationById(id, callback)</td>
    <td style="padding:15px">Get Access Profile Association by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessProfileAssociation(id, body, callback)</td>
    <td style="padding:15px">Update an existing Access Profile Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessProfileAssociation(id, callback)</td>
    <td style="padding:15px">Delete Access Profile Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessProfileAssociationSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/access-profile-association/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudBreakoutProfile(callback)</td>
    <td style="padding:15px">Get Cloud Breakout Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudBreakoutProfile(body, callback)</td>
    <td style="padding:15px">Add a new Cloud Breakout Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudBreakoutProfileById(id, callback)</td>
    <td style="padding:15px">Get Cloud Breakout Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudBreakoutProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Cloud Breakout Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudBreakoutProfile(id, callback)</td>
    <td style="padding:15px">Delete Cloud Breakout Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudBreakoutProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/cloud-breakout-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrlFile(callback)</td>
    <td style="padding:15px">Get Crl File list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCrlFile(body, callback)</td>
    <td style="padding:15px">Add a new Crl File</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrlFileById(id, callback)</td>
    <td style="padding:15px">Get Crl File by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCrlFile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Crl File</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCrlFile(id, callback)</td>
    <td style="padding:15px">Delete Crl File</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCrlFileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/crl-file/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomer(callback)</td>
    <td style="padding:15px">Get Customer list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomer(body, callback)</td>
    <td style="padding:15px">Add a new Customer</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerById(id, callback)</td>
    <td style="padding:15px">Get Customer by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomer(id, body, callback)</td>
    <td style="padding:15px">Update an existing Customer</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomer(id, callback)</td>
    <td style="padding:15px">Delete Customer</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomerSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/customer/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDvpnPreCompute(callback)</td>
    <td style="padding:15px">Get Dvpn Pre Compute list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDvpnPreCompute(body, callback)</td>
    <td style="padding:15px">Add a new Dvpn Pre Compute</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDvpnPreComputeById(id, callback)</td>
    <td style="padding:15px">Get Dvpn Pre Compute by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDvpnPreCompute(id, body, callback)</td>
    <td style="padding:15px">Update an existing Dvpn Pre Compute</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDvpnPreCompute(id, callback)</td>
    <td style="padding:15px">Delete Dvpn Pre Compute</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDvpnPreComputeSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/dvpn-pre-compute/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalNfvServiceProfile(callback)</td>
    <td style="padding:15px">Get Global Nfv Service Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalNfvServiceProfile(body, callback)</td>
    <td style="padding:15px">Add a new Global Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalNfvServiceProfileById(id, callback)</td>
    <td style="padding:15px">Get Global Nfv Service Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalNfvServiceProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Global Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalNfvServiceProfile(id, callback)</td>
    <td style="padding:15px">Delete Global Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalNfvServiceProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/global-nfv-service-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceInstance(callback)</td>
    <td style="padding:15px">Get Nfv Service Instance list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceInstance(body, callback)</td>
    <td style="padding:15px">Add a new Nfv Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceInstanceById(id, callback)</td>
    <td style="padding:15px">Get Nfv Service Instance by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNfvServiceInstance(id, body, callback)</td>
    <td style="padding:15px">Update an existing Nfv Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNfvServiceInstance(id, callback)</td>
    <td style="padding:15px">Delete Nfv Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceInstanceSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceInstanceBackup(callback)</td>
    <td style="padding:15px">Get Nfv Service Instance Backup list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceInstanceBackup(body, callback)</td>
    <td style="padding:15px">Add a new Nfv Service Instance Backup</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceInstanceBackupById(id, callback)</td>
    <td style="padding:15px">Get Nfv Service Instance Backup by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNfvServiceInstanceBackup(id, body, callback)</td>
    <td style="padding:15px">Update an existing Nfv Service Instance Backup</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNfvServiceInstanceBackup(id, callback)</td>
    <td style="padding:15px">Delete Nfv Service Instance Backup</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceInstanceBackupSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-instance-backup/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceProfile(callback)</td>
    <td style="padding:15px">Get Nfv Service Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceProfile(body, callback)</td>
    <td style="padding:15px">Add a new Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfvServiceProfileById(id, callback)</td>
    <td style="padding:15px">Get Nfv Service Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNfvServiceProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNfvServiceProfile(id, callback)</td>
    <td style="padding:15px">Delete Nfv Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNfvServiceProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/nfv-service-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPop(callback)</td>
    <td style="padding:15px">Get Pop list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPop(body, callback)</td>
    <td style="padding:15px">Add a new Pop</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPopById(id, callback)</td>
    <td style="padding:15px">Get Pop by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePop(id, body, callback)</td>
    <td style="padding:15px">Update an existing Pop</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePop(id, callback)</td>
    <td style="padding:15px">Delete Pop</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPopSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/pop/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortAuthProfile(callback)</td>
    <td style="padding:15px">Get Port Auth Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortAuthProfile(body, callback)</td>
    <td style="padding:15px">Add a new Port Auth Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortAuthProfileById(id, callback)</td>
    <td style="padding:15px">Get Port Auth Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePortAuthProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Port Auth Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortAuthProfile(id, callback)</td>
    <td style="padding:15px">Delete Port Auth Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortAuthProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-auth-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortConfigAssociation(callback)</td>
    <td style="padding:15px">Get Port Config Association list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortConfigAssociation(body, callback)</td>
    <td style="padding:15px">Add a new Port Config Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortConfigAssociationById(id, callback)</td>
    <td style="padding:15px">Get Port Config Association by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePortConfigAssociation(id, body, callback)</td>
    <td style="padding:15px">Update an existing Port Config Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortConfigAssociation(id, callback)</td>
    <td style="padding:15px">Delete Port Config Association</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortConfigAssociationSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-config-association/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortManualConfig(callback)</td>
    <td style="padding:15px">Get Port Manual Config list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortManualConfig(body, callback)</td>
    <td style="padding:15px">Add a new Port Manual Config</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortManualConfigById(id, callback)</td>
    <td style="padding:15px">Get Port Manual Config by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePortManualConfig(id, body, callback)</td>
    <td style="padding:15px">Update an existing Port Manual Config</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortManualConfig(id, callback)</td>
    <td style="padding:15px">Delete Port Manual Config</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortManualConfigSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-manual-config/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortProfile(callback)</td>
    <td style="padding:15px">Get Port Profile list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortProfile(body, callback)</td>
    <td style="padding:15px">Add a new Port Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortProfileById(id, callback)</td>
    <td style="padding:15px">Get Port Profile by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePortProfile(id, body, callback)</td>
    <td style="padding:15px">Update an existing Port Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortProfile(id, callback)</td>
    <td style="padding:15px">Delete Port Profile</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortProfileSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/port-profile/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPropertyMeta(callback)</td>
    <td style="padding:15px">Get Property Meta list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPropertyMeta(body, callback)</td>
    <td style="padding:15px">Add a new Property Meta</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPropertyMetaById(id, callback)</td>
    <td style="padding:15px">Get Property Meta by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePropertyMeta(id, body, callback)</td>
    <td style="padding:15px">Update an existing Property Meta</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePropertyMeta(id, callback)</td>
    <td style="padding:15px">Delete Property Meta</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPropertyMetaSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/property-meta/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusServer(callback)</td>
    <td style="padding:15px">Get Radius Server list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRadiusServer(body, callback)</td>
    <td style="padding:15px">Add a new Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusServerById(id, callback)</td>
    <td style="padding:15px">Get Radius Server by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRadiusServer(id, body, callback)</td>
    <td style="padding:15px">Update an existing Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRadiusServer(id, callback)</td>
    <td style="padding:15px">Delete Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRadiusServerSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/radius-server/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcePool(callback)</td>
    <td style="padding:15px">Get Resource Pool list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResourcePool(body, callback)</td>
    <td style="padding:15px">Add a new Resource Pool</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcePoolById(id, callback)</td>
    <td style="padding:15px">Get Resource Pool by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourcePool(id, body, callback)</td>
    <td style="padding:15px">Update an existing Resource Pool</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourcePool(id, callback)</td>
    <td style="padding:15px">Delete Resource Pool</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResourcePoolSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/resource-pool/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSite(callback)</td>
    <td style="padding:15px">Get Site list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSite(body, callback)</td>
    <td style="padding:15px">Add a new Site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteById(id, callback)</td>
    <td style="padding:15px">Get Site by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSite(id, body, callback)</td>
    <td style="padding:15px">Update an existing Site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSite(id, callback)</td>
    <td style="padding:15px">Delete Site</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteTemplate(callback)</td>
    <td style="padding:15px">Get Site Template list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteTemplate(body, callback)</td>
    <td style="padding:15px">Add a new Site Template</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteTemplateById(id, callback)</td>
    <td style="padding:15px">Get Site Template by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteTemplate(id, body, callback)</td>
    <td style="padding:15px">Update an existing Site Template</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteTemplate(id, callback)</td>
    <td style="padding:15px">Delete Site Template</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteTemplateSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/site-template/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWifiEms(callback)</td>
    <td style="padding:15px">Get Wifi Ems list</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWifiEms(body, callback)</td>
    <td style="padding:15px">Add a new Wifi Ems</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWifiEmsById(id, callback)</td>
    <td style="padding:15px">Get Wifi Ems by id</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWifiEms(id, body, callback)</td>
    <td style="padding:15px">Update an existing Wifi Ems</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWifiEms(id, callback)</td>
    <td style="padding:15px">Delete Wifi Ems</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWifiEmsSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/tssm/wifi-ems/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPslamAppSlaMappings(body, callback)</td>
    <td style="padding:15px">query the Application-SLA mappings</td>
    <td style="padding:15px">{base_path}/{version}/pslam/get_app_sla_mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPslamSlaByUuids(body, callback)</td>
    <td style="padding:15px">get complete details of SLA objects coressponding to the elements in the UUID list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/get_sla_by_uuids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPslamSlaHistory(body, callback)</td>
    <td style="padding:15px">fetch the historical data for a given SLA</td>
    <td style="padding:15px">{base_path}/{version}/pslam/get_sla_history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppGroup(callback)</td>
    <td style="padding:15px">Get App Group list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppGroup(body, callback)</td>
    <td style="padding:15px">Add a new App Group</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppGroupById(id, callback)</td>
    <td style="padding:15px">Get App Group by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppGroup(id, body, callback)</td>
    <td style="padding:15px">Update an existing App Group</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppGroup(id, callback)</td>
    <td style="padding:15px">Delete App Group</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppGroupSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app-group/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppOperationalInfo(callback)</td>
    <td style="padding:15px">Get App Operational Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppOperationalInfo(body, callback)</td>
    <td style="padding:15px">Add a new App Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppOperationalInfoById(id, callback)</td>
    <td style="padding:15px">Get App Operational Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppOperationalInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing App Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppOperationalInfo(id, callback)</td>
    <td style="padding:15px">Delete App Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppOperationalInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/app_operational_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplication(callback)</td>
    <td style="padding:15px">Get Application list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplication(body, callback)</td>
    <td style="padding:15px">Add a new Application</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationById(id, callback)</td>
    <td style="padding:15px">Get Application by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplication(id, body, callback)</td>
    <td style="padding:15px">Update an existing Application</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplication(id, callback)</td>
    <td style="padding:15px">Delete Application</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/application/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategory(callback)</td>
    <td style="padding:15px">Get Category list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCategory(body, callback)</td>
    <td style="padding:15px">Add a new Category</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategoryById(id, callback)</td>
    <td style="padding:15px">Get Category by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCategory(id, body, callback)</td>
    <td style="padding:15px">Update an existing Category</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCategory(id, callback)</td>
    <td style="padding:15px">Delete Category</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCategorySearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/category/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCosOperationalInfo(callback)</td>
    <td style="padding:15px">Get Cos Operational Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCosOperationalInfo(body, callback)</td>
    <td style="padding:15px">Add a new Cos Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCosOperationalInfoById(id, callback)</td>
    <td style="padding:15px">Get Cos Operational Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCosOperationalInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Cos Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCosOperationalInfo(id, callback)</td>
    <td style="padding:15px">Delete Cos Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCosOperationalInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/cos_operational_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJob(callback)</td>
    <td style="padding:15px">Get Job list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJob(body, callback)</td>
    <td style="padding:15px">Add a new Job</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobById(id, callback)</td>
    <td style="padding:15px">Get Job by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJob(id, body, callback)</td>
    <td style="padding:15px">Update an existing Job</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJob(id, callback)</td>
    <td style="padding:15px">Delete Job</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJobSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/job/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkMonitorInfo(callback)</td>
    <td style="padding:15px">Get Link Monitor Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLinkMonitorInfo(body, callback)</td>
    <td style="padding:15px">Add a new Link Monitor Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkMonitorInfoById(id, callback)</td>
    <td style="padding:15px">Get Link Monitor Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLinkMonitorInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Link Monitor Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLinkMonitorInfo(id, callback)</td>
    <td style="padding:15px">Delete Link Monitor Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLinkMonitorInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/link_monitor_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverlayPathInfo(callback)</td>
    <td style="padding:15px">Get Overlay Path Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOverlayPathInfo(body, callback)</td>
    <td style="padding:15px">Add a new Overlay Path Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverlayPathInfoById(id, callback)</td>
    <td style="padding:15px">Get Overlay Path Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOverlayPathInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Overlay Path Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOverlayPathInfo(id, callback)</td>
    <td style="padding:15px">Delete Overlay Path Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOverlayPathInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/overlay_path_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDef(callback)</td>
    <td style="padding:15px">Get Policy Def list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyDef(body, callback)</td>
    <td style="padding:15px">Add a new Policy Def</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDefById(id, callback)</td>
    <td style="padding:15px">Get Policy Def by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyDef(id, body, callback)</td>
    <td style="padding:15px">Update an existing Policy Def</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyDef(id, callback)</td>
    <td style="padding:15px">Delete Policy Def</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyDefSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_def/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDeploymentInfo(callback)</td>
    <td style="padding:15px">Get Policy Deployment Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyDeploymentInfo(body, callback)</td>
    <td style="padding:15px">Add a new Policy Deployment Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDeploymentInfoById(id, callback)</td>
    <td style="padding:15px">Get Policy Deployment Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyDeploymentInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Policy Deployment Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyDeploymentInfo(id, callback)</td>
    <td style="padding:15px">Delete Policy Deployment Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyDeploymentInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_deployment_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyOperationalInfo(callback)</td>
    <td style="padding:15px">Get Policy Operational Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyOperationalInfo(body, callback)</td>
    <td style="padding:15px">Add a new Policy Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyOperationalInfoById(id, callback)</td>
    <td style="padding:15px">Get Policy Operational Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyOperationalInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Policy Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyOperationalInfo(id, callback)</td>
    <td style="padding:15px">Delete Policy Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyOperationalInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/policy_operational_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProbeParams(callback)</td>
    <td style="padding:15px">Get ProbeParams list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProbeParams(body, callback)</td>
    <td style="padding:15px">Add a new Probe Params</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProbeParamsById(id, callback)</td>
    <td style="padding:15px">Get Probe Params by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProbeParams(id, body, callback)</td>
    <td style="padding:15px">Update an existing Probe Params</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProbeParams(id, callback)</td>
    <td style="padding:15px">Delete Probe Params</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProbeParamsSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/probe_params/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRulesOperationalInfo(callback)</td>
    <td style="padding:15px">Get Rules Operational Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRulesOperationalInfo(body, callback)</td>
    <td style="padding:15px">Add a new Rules Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRulesOperationalInfoById(id, callback)</td>
    <td style="padding:15px">Get Rules Operational Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRulesOperationalInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Rules Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRulesOperationalInfo(id, callback)</td>
    <td style="padding:15px">Delete Rules Operational Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRulesOperationalInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/rules_operational_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSla(callback)</td>
    <td style="padding:15px">Get Sla list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSla(body, callback)</td>
    <td style="padding:15px">Add a new Sla</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaById(id, callback)</td>
    <td style="padding:15px">Get Sla by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSla(id, body, callback)</td>
    <td style="padding:15px">Update an existing Sla</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSla(id, callback)</td>
    <td style="padding:15px">Delete Sla</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaArchive(callback)</td>
    <td style="padding:15px">Get Sla Archive list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaArchive(body, callback)</td>
    <td style="padding:15px">Add a new Sla Archive</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaArchiveById(id, callback)</td>
    <td style="padding:15px">Get Sla Archive by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSlaArchive(id, body, callback)</td>
    <td style="padding:15px">Update an existing Sla Archive</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSlaArchive(id, callback)</td>
    <td style="padding:15px">Delete Sla Archive</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaArchiveSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_archive/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaMgmtInfo(callback)</td>
    <td style="padding:15px">Get Sla Mgmt Info list</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaMgmtInfo(body, callback)</td>
    <td style="padding:15px">Add a new Sla Mgmt Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaMgmtInfoById(id, callback)</td>
    <td style="padding:15px">Get Sla Mgmt Info by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSlaMgmtInfo(id, body, callback)</td>
    <td style="padding:15px">Update an existing Sla Mgmt Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSlaMgmtInfo(id, callback)</td>
    <td style="padding:15px">Delete Sla Mgmt Info</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaMgmtInfoSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_mgmt_info/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaToVrfMapping(callback)</td>
    <td style="padding:15px">Get Sla To Vrf Mapping</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaToVrfMapping(body, callback)</td>
    <td style="padding:15px">Add a new Sla To Vrf Mapping</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSlaToVrfMappingById(id, callback)</td>
    <td style="padding:15px">Get Sla To Vrf Mapping by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSlaToVrfMapping(id, body, callback)</td>
    <td style="padding:15px">Update an existing Sla To Vrf Mapping</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSlaToVrfMapping(id, callback)</td>
    <td style="padding:15px">Delete Sla To Vrf Mapping</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSlaToVrfMappingSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/sla_to_vrf_mapping/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficType(callback)</td>
    <td style="padding:15px">Get TrafficType</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTrafficType(body, callback)</td>
    <td style="padding:15px">Add a new Traffic Type</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficTypeById(id, callback)</td>
    <td style="padding:15px">Get TrafficType by id</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTrafficType(id, body, callback)</td>
    <td style="padding:15px">Update an existing Traffic Type</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrafficType(id, callback)</td>
    <td style="padding:15px">Delete Traffic Type</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTrafficTypeSearch(body, callback)</td>
    <td style="padding:15px">Api to make elastic search _search queries for objects</td>
    <td style="padding:15px">{base_path}/{version}/pslam/traffic_type/_search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
