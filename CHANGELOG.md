
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:53PM

See merge request itentialopensource/adapters/adapter-juniper_cso!13

---

## 0.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-juniper_cso!11

---

## 0.4.2 [08-15-2024]

* Changes made at 2024.08.14_19:11PM

See merge request itentialopensource/adapters/adapter-juniper_cso!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:26PM

See merge request itentialopensource/adapters/adapter-juniper_cso!9

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!8

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:44PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_12:44PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:00PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:30AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!4

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!3

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!2

---

## 0.1.2 [03-08-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_cso!1

---

## 0.1.1 [09-10-2020]

- Initial Commit

See commit a694a0a

---
