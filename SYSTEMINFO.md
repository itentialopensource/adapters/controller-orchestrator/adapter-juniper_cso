# Juniper Contrail Services Orchestrator

Vendor: Juniper
Homepage: https://www.juniper.net/us/en.html

Product: Contrail Services Orchestrator
Product Page: https://www.juniper.net/documentation/product/us/en/contrail-service-orchestration/

## Introduction
We classify Juniper CSO into the Data Center and Network Services domais as Juniper CSO provides the capability to get information about and make changes to infrastructure.

"Juniper Networks Contrail Service Orchestration (CSO) transforms traditional branch networks, offering opportunities for high flexibility of the network, rapid introduction of new services, automation of network administration, and cost savings."

## Why Integrate
The Juniper CSO adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper CSO. With this adapter you have the ability to perform operations such as:

- Policy Management
- PSLAM
- TSSM

## Additional Product Documentation
The [Juniper CSO API Documentation](https://www.juniper.net/documentation/us/en/software/cso6.2.0/api-guide/secmgt-ecm.html)
The [Juniper CSO API Keystone AUthentication](https://www.juniper.net/documentation/us/en/software/cso6.3.0/api-guide/index.html#authentication-using-keystone)